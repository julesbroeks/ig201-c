﻿using UnityEngine;
using System.Collections;

public class movementJetski : MonoBehaviour
{

		public int moveSpeedJetski = 2;
		public GameObject dummyJetski;

		// Use this for initialization
		void Start ()
		{
		}
	
		// Update is called once per frame
		void Update ()
		{
		
				//links en rechts
				if (Input.GetKey (KeyCode.A) && transform.position.x > -100.15) {
						transform.Translate (new Vector3 (-moveSpeedJetski, 0, 0) * Time.deltaTime * moveSpeedJetski);
				} 
				if (Input.GetKey (KeyCode.D) && transform.position.x < -75.83) {
						transform.Translate (new Vector3 (moveSpeedJetski, 0, 0) * Time.deltaTime * moveSpeedJetski);
				}
				//op en neer
				if (Input.GetKey (KeyCode.W) && transform.position.y < 327.48f && transform.position.z < 146.17f) {
						transform.Translate (new Vector3 (0, 0, moveSpeedJetski) * Time.deltaTime * moveSpeedJetski);
				} 
				if (Input.GetKey (KeyCode.S) && transform.position.y > 300.15f && transform.position.z > 129.81f) {
						transform.Translate (new Vector3 (0, 0, -moveSpeedJetski) * Time.deltaTime * moveSpeedJetski);
				} 
		
				rigidbody.velocity = (new Vector3 (0, 0, 0));
		}
}
