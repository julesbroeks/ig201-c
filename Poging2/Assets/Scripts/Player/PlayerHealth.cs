﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{

		public int Totalhealth = 100;
		public static bool doublePoints = false;
		// Use this for initialization
		void Start ()
		{
	
				RPCManager.missedCrate += this.DeductHealth;
				RPCManager.missedTheRing += this.DeductHealth;
				RPCManager.addPointsForRing += this.AddHealth;
		}

		public void DeductHealth (int health)
		{
				GetComponent<PhotonView> ().RPC ("DeductHealthRPC", PhotonTargets.All, new object[] {health});
		}

		public void AddHealth (int health)
		{
				GetComponent<PhotonView> ().RPC ("AddHealthRPC", PhotonTargets.All, new object[] {health});
		}

		[RPC]
		void DeductHealthRPC (int health)
		{
				Totalhealth -= health;
				UpdateHealth ();
		}

		[RPC]
		void AddHealthRPC (int health)
		{
				if (Totalhealth < 200) {
						Totalhealth += health;
						UpdateHealth ();
				}
		}

		void UpdateHealth ()
		{
				GameObject.FindGameObjectWithTag ("playerHealth").GetComponent<Text> ().text = Totalhealth.ToString ();
		}


}
