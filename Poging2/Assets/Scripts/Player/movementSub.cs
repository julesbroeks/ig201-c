﻿using UnityEngine;
using System.Collections;

public class movementSub : MonoBehaviour
{

		public float moveSpeed = 2f;


		// Use this for initialization
		void Start ()
		{
		}
	
		// Update is called once per frame
		void Update ()
		{

				//links en rechts
				if (Input.GetKey (KeyCode.A)) {
						transform.Translate (new Vector3 (moveSpeed, 0, 0) * Time.deltaTime * moveSpeed);
				}
				if (Input.GetKey (KeyCode.D)) {
						transform.Translate (new Vector3 (-moveSpeed, 0, 0) * Time.deltaTime * moveSpeed);
				}
				//op en neer
				if (Input.GetKey (KeyCode.W)) {
						transform.Translate (new Vector3 (0, 0, moveSpeed) * Time.deltaTime * moveSpeed);
				} 
				if (Input.GetKey (KeyCode.S)) {
						transform.Translate (new Vector3 (0, 0, -moveSpeed) * Time.deltaTime * moveSpeed);
				} 

						rigidbody.velocity = (new Vector3 (0, 0, 0));
				}

}
