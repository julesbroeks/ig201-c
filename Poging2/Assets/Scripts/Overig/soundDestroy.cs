﻿using UnityEngine;
using System.Collections;

public class soundDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (DeleteSound ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator DeleteSound(){
		while (true) {
			yield return new WaitForSeconds (2f);

			if(GetComponent<PhotonView>().isMine)
				PhotonNetwork.Destroy(this.gameObject);
		}
	}
}
