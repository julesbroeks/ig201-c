﻿using UnityEngine;
using System.Collections;

public class UpgradeManager : MonoBehaviour
{


		public delegate void powerUpEventManager (string powerUp);

		public static event powerUpEventManager powerUpReceived;
		public static event powerUpEventManager powerUpLost;
	
		public static void ReceivedNewPowerUp (string powerUp)
		{
				if (powerUpReceived != null) {
						powerUpReceived (powerUp);
				}
		
		}

		public static void DeletePowerUp (string powerUp)
		{
				if (powerUpLost != null) {
						powerUpLost (powerUp);
				}
		
		}

}
