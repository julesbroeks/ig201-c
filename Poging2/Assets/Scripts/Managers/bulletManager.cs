﻿using UnityEngine;
using System.Collections;

public class bulletManager : MonoBehaviour
{

		RecycleBullets bulletRecycling;
		public GameObject submarine;
		public static string shotType = "normal";
		public static Vector3 subPosition;
		public Camera submarineCamera;



		// sets the static vector equal to the sub position vector

		void Start ()
		{
				UpgradeManager.powerUpReceived += this.NewPowerUpReceived;
				UpgradeManager.powerUpLost += this.PowerUpLost;
		}

		public void NewPowerUpReceived (string powerUp)
		{
				if (powerUp == "seekingMissle") {
						shotType = "seeking";
				}
		}

		public void PowerUpLost (string powerUp)
		{
				if (powerUp == "seekingMissle") {
						shotType = "normal";
				}
		}

		void Update ()
		{
				subPosition = submarine.transform.position;

		}
		// stards the chain of firing a bullet depending on the kind of bullet.
		public static void FireBullet (Vector3 direction, GameObject targetObject, bool hasTarget)
		{
				switch (shotType) {
				case "normal": 

						if (bullets.bulletGarbage.Count > 0) {
								RecycleBullets.recycleNormalBulletCreateNew (direction);
						} else {
								CreateBullets.createNormalNewBullet (direction);
						}
						break;

				case "seeking":

			 
						if (seekingBulletes.seekingBulletGarbage.Count > 0) {
								RecycleBullets.recycleSeekingBulletCreateNew (direction, targetObject, hasTarget);
						} else {
								CreateBullets.createSeekingNewBullet (direction, targetObject, hasTarget);
						}
						break;
				}
		  

		}


	
	

}


