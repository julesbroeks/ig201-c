﻿using UnityEngine;
using System.Collections;

public class r_playerSetupManager : MonoBehaviour
{

		// Use this for initialization
		private string currentGameState;
		public GameObject submarine;
		public GameObject jetski;
		public GameObject camsubmarine;
		public GameObject camjetski;
		public GameObject MINIsubCamera;
		public GameObject MINIjetskiCamera;

		void Awake ()
		{
				// set everything to enabled
				MINIjetskiCamera.SetActive (true);
				camsubmarine.SetActive (true);
				camjetski.SetActive (true);
				MINIsubCamera.SetActive (true);
				submarine.GetComponent<movementSub> ().enabled = true;
				jetski.GetComponent<movementJetski> ().enabled = true;

		
		}

		void Start ()
		{


				SetupJetski ();
				SetupSub ();
		

		}
		// Update is called once per frame
		void Update ()
		{
	

		}

		void SetupJetski ()
		{


		
	
	
	
				if (PhotonNetwork.isMasterClient) {
			
						PhotonNetwork.Instantiate ("tempJetski", jetski.transform.position, jetski.transform.rotation, 0);
						PhotonNetwork.Instantiate ("jetskiCamera", camjetski.transform.position, camjetski.transform.rotation, 0);
						PhotonNetwork.Instantiate ("MINIsubCamera", MINIsubCamera.transform.position, MINIsubCamera.transform.rotation, 0);


						jetski.GetComponent<movementJetski> ().enabled = true;
						submarine.GetComponent<movementSub> ().enabled = false;
	
						camjetski.SetActive (true);
						MINIsubCamera.SetActive (true);
						camsubmarine.SetActive (false);
						MINIjetskiCamera.SetActive (false);
						Debug.Log ("setup of jetski is called completely");
				}

	

		
	
		}

		void SetupSub ()
		{

	
				if (!PhotonNetwork.isMasterClient) {

						PhotonNetwork.Instantiate ("tempSub", submarine.transform.position, submarine.transform.rotation, 0);
						PhotonNetwork.Instantiate ("subCamera", camsubmarine.transform.position, camsubmarine.transform.rotation, 0);
						PhotonNetwork.Instantiate ("MINIjetskiCamera", MINIjetskiCamera.transform.position, MINIjetskiCamera.transform.rotation, 0);
		
		
						submarine.GetComponent<movementSub> ().enabled = true;
						jetski.GetComponent<movementJetski> ().enabled = false;

						MINIjetskiCamera.SetActive (true);
						camsubmarine.SetActive (true);
						camjetski.SetActive (false);
						MINIsubCamera.SetActive (false);
						Debug.Log ("setup of sub is called completely");

				}


		}
}
