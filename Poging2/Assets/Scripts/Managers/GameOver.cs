﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour
{


		PlayerHealth playerHealth;
		GameObject gameOver;
		public GameObject soundManager;
		bool gameover;

		// Use this for initialization
		void Start ()
		{
				//playerHealth = GameObject.FindGameObjectWithTag("RPCmanager").GetComponent<PlayerHealth>();
	
				soundManager.SetActive (true); 
				gameOver = GameObject.FindGameObjectWithTag ("GameOverCamera");
				gameOver.SetActive (false);
		}
	
		// Update is called once per frame
		void Update ()
		{


				if (playerHealth == null) {
						playerHealth = GameObject.FindGameObjectWithTag ("RPCmanager").GetComponent<PlayerHealth> ();
				}

				if (playerHealth.Totalhealth <= 0) {
						gameOver.SetActive (true);
						gameover = true;
	
						soundManager.SetActive (false); 
				}
				if (Input.GetKeyDown (KeyCode.R)) {
						ReturnToLobby ();
				}
				if (PhotonNetwork.room.playerCount == 1) {
			
						PhotonNetwork.LoadLevel (0);
						PhotonNetwork.LeaveRoom ();
						PhotonNetwork.LoadLevel (0);
		
			
				}


		}

		public void ReturnToLobby ()
		{

				PhotonNetwork.LoadLevel (0);
				PhotonNetwork.LeaveRoom ();
				PhotonNetwork.LoadLevel (0);




		}

		public void QuitGame ()
		{
				Application.Quit ();
		}
}
