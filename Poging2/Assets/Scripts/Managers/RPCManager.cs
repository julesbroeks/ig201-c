﻿using UnityEngine;
using System.Collections;

public class RPCManager : MonoBehaviour
{



		public delegate void pointsHitMiss (int punten);

		public static event pointsHitMiss missedTheRing;
		public static event pointsHitMiss missedCrate;
		public static event pointsHitMiss addPointsForRing;
		public static event pointsHitMiss hitRing;
		public static event pointsHitMiss hitCoin;

		public delegate void sinkCrate (int sink);

		public static event sinkCrate sinkIt;

		public static void MissRingFunction (int punten)
		{
				if (missedTheRing != null) {
						missedTheRing (punten);
				}
		}

		public static void MissCrateFunction (int punten)
		{
				if (missedCrate != null) {
						missedCrate (punten);
				}
		}

		public static void AddPoints (int punten)
		{
				if (addPointsForRing != null) {
						addPointsForRing (punten);
				}

		}

		public static void SinkCrateFunction (int sink)
		{
				if (sinkIt != null) {
						sinkIt (sink);
				}
		}
}
