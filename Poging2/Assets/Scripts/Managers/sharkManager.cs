﻿using UnityEngine;
using System.Collections;

public class sharkManager : MonoBehaviour
{


		public delegate void sharkSpawn (GameObject sharkSpawn);

		public static event sharkSpawn newSharkSpawned;

		public static void SharkSpawned (GameObject shark)
		{
				if (newSharkSpawned != null) {
						newSharkSpawned (shark);
				}
		}
}
