﻿using UnityEngine;
using System.Collections;

public class r_ObstakleAndEnemySetup : MonoBehaviour
{

		GameObject sub;
		public GameObject coin;
		public GameObject obstacle;
		public static float spawnTime = 1.4f;
	
		// Use this for initialization
		void Start ()
		{

				if (PhotonNetwork.isMasterClient) {
						StartCoroutine (CreateObstacle ());
						StartCoroutine (CreateCoins (2));
				}


				if (!PhotonNetwork.isMasterClient) {
						StartCoroutine (SetupRings ());	
				}


		}
	
		// Update is called once per frame
		void Update ()
		{
				sub = GameObject.FindGameObjectWithTag ("sub");
				if (!PhotonNetwork.isMasterClient) {
						SinkTheCrate ();
				}
		}

		IEnumerator  SetupRings ()
		{
				while (true) {
						PhotonNetwork.Instantiate ("ring", new Vector3 (Random.Range (8, -4), Random.Range (9, 23), -120), Quaternion.identity, 0);
						yield return new WaitForSeconds (5);
				}
		}
		
		IEnumerator CreateObstacle ()
		{
				while (true) {
						PhotonNetwork.Instantiate ("playerObstacle", new Vector3 (Random.Range (-95, -80), 336.48f, 154.38f), obstacle.transform.rotation, 0);
						yield return new WaitForSeconds (spawnTime);
				}
		}
		
		IEnumerator CreateCoins (int time)
		{
				while (true) {
						PhotonNetwork.Instantiate ("collectingObj", new Vector3 (Random.Range (-95, -80), 336.48f, 154.38f), coin.transform.rotation, 0);
						yield return new WaitForSeconds (time);
				}
		}

		public void SinkTheCrate ()
		{

				if (sinkCrate.sinking == true) {
						PhotonNetwork.Instantiate ("crateSub", new Vector3 (sub.transform.position.x, sub.transform.position.y + 8, -100), Quaternion.identity, 0);
						sinkCrate.sinking = false;
				}
		}
}
