//Jules Broeks//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

// Handles all management within the Network UI.
public class r_networkManager : MonoBehaviour
{

		string playerRoomName = "" ;
		bool readyToLoad = false;
		// Panels
		private GameObject GameLobbyPanel;
		private GameObject GameCreatePanel;
		private GameObject ConnectionDisplayPanel;
		private GameObject WaitingForPlayersPanel;
		private GameObject Instructions;
		// GameLobbyGameListPanels

		private List<GameObject> buttons = new List<GameObject> ();
		private GameObject GameListButton1, GameListButton2, GameListButton3, GameListButton4, GameListButton5;
		private GameObject GameNameGame1, GameNameGame2, GameNameGame3, GameNameGame4, GameNameGame5;
	
		// UI Texts
		private GameObject GameLobbyNoGamesText;
		private GameObject ConnectionDisplayPanelText;

		// UI Inputs
		private GameObject GameNameInput;
		public static string playerState;
		// UI buttons

		// NetwerkInterface States && previous state
		string NetworkInterfaceState = "connectionDisplayPanel";
		bool hasJoinedRoom = false;
		private int PickPlayer;

		void Awake ()
		{
				// Panels

				readyToLoad = false;
				ConnectionDisplayPanel = GameObject.FindGameObjectWithTag ("ConnectionDisplayPanel");
				GameLobbyPanel = GameObject.FindGameObjectWithTag ("GameLobbyPanel");
				GameCreatePanel = GameObject.FindGameObjectWithTag ("GameCreatePanel");
				WaitingForPlayersPanel = GameObject.FindGameObjectWithTag ("WaitingForPlayersPanel");
				Instructions = GameObject.FindGameObjectWithTag ("Instructions");
				// Texts
				GameLobbyNoGamesText = GameObject.FindGameObjectWithTag ("GameLobbyNoGamesText");
				ConnectionDisplayPanelText = GameObject.FindGameObjectWithTag ("ConnectionDisplayPanelText");
				// inputs
				GameNameInput = GameObject.FindGameObjectWithTag ("GameNameInput");

				// GameListButtons
				GameListButton1 = GameObject.FindGameObjectWithTag ("GameListButton1");
				GameListButton2 = GameObject.FindGameObjectWithTag ("GameListButton2");
				GameListButton3 = GameObject.FindGameObjectWithTag ("GameListButton3");
				GameListButton4 = GameObject.FindGameObjectWithTag ("GameListButton4");
				GameListButton5 = GameObject.FindGameObjectWithTag ("GameListButton5");
				// Gamelistbutton NAMES ** Used for joining game  ** 
				GameNameGame1 = GameObject.FindGameObjectWithTag ("GameNameGame1");
				GameNameGame2 = GameObject.FindGameObjectWithTag ("GameNameGame2");
				GameNameGame3 = GameObject.FindGameObjectWithTag ("GameNameGame3");
				GameNameGame4 = GameObject.FindGameObjectWithTag ("GameNameGame4");
				GameNameGame5 = GameObject.FindGameObjectWithTag ("GameNameGame5");
				// wether or not to show the button initialised as hidden
				GameListButton1.SetActive (false);
				GameListButton2.SetActive (false);
				GameListButton3.SetActive (false);
				GameListButton4.SetActive (false);
				GameListButton5.SetActive (false);
	 	


		}

		// Use this for initialization
		void Start ()
		{
				Screen.showCursor = true;
				readyToLoad = false;
				// adds the 5 game buttons ( panels) to the array	
				buttons.Add (GameListButton1);
				buttons.Add (GameListButton2);
				buttons.Add (GameListButton3);
				buttons.Add (GameListButton4);
				buttons.Add (GameListButton5); 
				// displays the correct Panel
				DisplayConnectionStatePanel ();
				//Connects to the server
				Connect ();

		}

		void Connect ()
		{
				// Sets State To connecting for handling the panel management
				NetworkInterfaceState = "connecting";
				// connetcs using settings ( the string is version ) change on version change to disable with newer version.
				PhotonNetwork.ConnectUsingSettings ("V.1.0");

		}

		void DisplayGameLobby ()
		{

				// shows and hides the pannels respectively.
				GameCreatePanel.SetActive (false);
				GameLobbyPanel.SetActive (true);
				ConnectionDisplayPanel.SetActive (false);
				WaitingForPlayersPanel.SetActive (false);
				Instructions.SetActive (false);

				// diplays 
				DisplayRoomList (); // displays the no games available text if no games 
				DisplayRoomListButtons ();
		
		}
		// Sets up to 5 buttons to active depending on how many rooms exist ( should be edited to rooms available to JOIN ) later not nessesary for gameplay

		void DisplayRoomListButtons ()
		{
	

				switch (PhotonNetwork.countOfRooms) {

				case 0:  
						GameListButton1.SetActive (false);
						GameListButton2.SetActive (false);
						GameListButton3.SetActive (false);
						GameListButton4.SetActive (false);
						GameListButton5.SetActive (false);
						break;
				case 1: 
						GameListButton1.SetActive (true);
						GameListButton2.SetActive (false);
						GameListButton3.SetActive (false);
						GameListButton4.SetActive (false);
						GameListButton5.SetActive (false);
						break;
				case 2: 
						GameListButton1.SetActive (true);
						GameListButton2.SetActive (true);
						GameListButton3.SetActive (false);
						GameListButton4.SetActive (false);
						GameListButton5.SetActive (false);
						break;
				case 3:
						GameListButton1.SetActive (true);
						GameListButton2.SetActive (true);
						GameListButton3.SetActive (true);
						GameListButton4.SetActive (false);
						GameListButton5.SetActive (false);
						break;
				case 4: 
						GameListButton1.SetActive (true);
						GameListButton2.SetActive (true);
						GameListButton3.SetActive (true);
						GameListButton4.SetActive (true);
						GameListButton5.SetActive (false);
						break;
				case 5:
						GameListButton1.SetActive (true);
						GameListButton2.SetActive (true);
						GameListButton3.SetActive (true);
						GameListButton4.SetActive (true);
						GameListButton5.SetActive (true);
						break;

				}
				if (PhotonNetwork.countOfRooms > 0) {
						SetGameListButtons (PhotonNetwork.countOfRooms);
				}
		}

		// sets the name of the games in the button labels
		void SetGameListButtons (int a)
		{

				if (!hasJoinedRoom) {
						if (a > 4) {
								a = 4;
						}
						a = a - 1;
						switch (a) {

						case 0:
								try {
										GameNameGame1.GetComponent<Text> ().text = PhotonNetwork.GetRoomList () [0].name;
								} catch {
										break;
								}
								break;
						case 1:
								try {
										GameNameGame2.GetComponent<Text> ().text = PhotonNetwork.GetRoomList () [1].name;
										SetGameListButtons (a);
										break;
								} catch {
										break;
								}

							
						case 2:
								try {
										GameNameGame3.GetComponent<Text> ().text = PhotonNetwork.GetRoomList () [2].name;
										SetGameListButtons (a);
										break;
								} catch {
										break;
								}
						case 3:
								try {
										GameNameGame4.GetComponent<Text> ().text = PhotonNetwork.GetRoomList () [3].name;
										SetGameListButtons (a);
										break;
								} catch {
										break;
								}
						case 4:
								try {
										GameNameGame5.GetComponent<Text> ().text = PhotonNetwork.GetRoomList () [4].name;
										SetGameListButtons (a);
										break;
								} catch {
										break;
								}
		
		
						}
				}


		}

		// gets clicked if the joingame button in the list is clicked and gives as parameter the Gameobject  TEXT 
		// of that same button panel, this so that this can ( and will ) be used for recovering the name from the button
		// finally joins the room with the given Roomname;
		public void JoinGameButtonListGameClick (GameObject buttonObjectName)
		{


				hasJoinedRoom = true;
				string roomToJoin = buttonObjectName.GetComponent<Text> ().text;


				PhotonNetwork.JoinRoom (roomToJoin);
		}

		//  if any games exist set the gameLobby Text to false, if there are no games, the text no games available will be shown 
		void DisplayRoomList ()
		{


				if (PhotonNetwork.countOfRooms > 0) {				
						GameLobbyNoGamesText.SetActive (false);

				} else {
						GameLobbyNoGamesText.SetActive (true);
						GameLobbyNoGamesText.GetComponent<Text> ().text = "No Games Available at the moment. Please Create your own game or try agian later.";
				}
		}


		//  sets the connectionStatePanel visible and the rest invisible,
		// in this panel the welcome message is shown and any message if not able to connect. and if connected this is shown ofcourse

		void DisplayConnectionStatePanel ()
		{		

				GameCreatePanel.SetActive (false);
				GameLobbyPanel.SetActive (false);
				WaitingForPlayersPanel.SetActive (false);
				ConnectionDisplayPanel.SetActive (true);
				Instructions.SetActive (false);
				// Gives Welcome Message is Succesfully Connected to Lobby
				if ((PhotonNetwork.connectionStateDetailed.ToString ()) == "PeerCreated") {
						ConnectionDisplayPanelText.GetComponent<Text> ().text = "Welcome ! You have succesfully Connected to the Lobby.";
				} else {
						ConnectionDisplayPanelText.GetComponent<Text> ().text = (PhotonNetwork.connectionStateDetailed.ToString ());
				}
		}

		// this makes the Create Game panel visible and disables the other panels.
		void DisplayCreateGame ()
		{

				GameCreatePanel.SetActive (true);
				GameLobbyPanel.SetActive (false);
				ConnectionDisplayPanel.SetActive (false);
				WaitingForPlayersPanel.SetActive (false);
				Instructions.SetActive (false);
		
		}

		//  shows the waitforplayerspanel and dsisables the rest
		void DisplayWaitingForPlayersPanel ()
		{

				GameCreatePanel.gameObject.SetActive (false);
				GameLobbyPanel.gameObject.SetActive (false);
				ConnectionDisplayPanel.gameObject.SetActive (false);
				WaitingForPlayersPanel.SetActive (true);
				Instructions.SetActive (false);
		}
		// gets fired if the back button on the waiting for player panle is clicked , leaves the current room and ets the networdInterfacdeState to gamelobbypanel
		public void WaitingForPlayersPanelBackClicked ()
		{
				PhotonNetwork.LeaveRoom ();
				NetworkInterfaceState = "GameLobbyPanel";
				hasJoinedRoom = false;
			

		}
// Update method explained in parts
		void Update ()
		{
				// if im in a room and this room has more than 1 people ( so 2)   it will load level 1
				if (PhotonNetwork.room != null) {

						if (PhotonNetwork.room.playerCount == 2 && readyToLoad == true) {

								
		
			
								PhotonNetwork.LoadLevel (1);
										
						}
				}
				// switches between netwerk interface states
				
				switch (NetworkInterfaceState) {

				case "connectionDisplayPanel":
						DisplayConnectionStatePanel ();
						break;
				case "GameLobbyPanel":
						DisplayGameLobby ();
						break;
				case "GameCreatePanel":
						DisplayCreateGame ();
						break;
				case "WaitingForPlayersPanel":
						DisplayWaitingForPlayersPanel ();
						break;
				case "InstructionsPanel":
						InstructionsScreen ();
						break;
				}


	


		}

		void OnLeftRoom ()
		{
				PhotonNetwork.LoadLevel (0);
		}

		// fired on joining the lobby not used yet but will most likely be used.
		void OnJoinedLobby ()
		{
	
		}

		// fired on joining a room stes the waiting panel the correct visibility true the state machine
		void OnJoinedRoom ()
		{
				hasJoinedRoom = true;
				NetworkInterfaceState = "WaitingForPlayersPanel";


		}


		// gets first on clicking create game button in the main menu, this will leave the current room ( if in any else event won't fire ) 
		// and set hasjoinedroom to false and set the create game menu visible to the player
		public void CreateGameButtonClick ()
		{
				PhotonNetwork.LeaveRoom ();
				hasJoinedRoom = false;
				NetworkInterfaceState = "GameCreatePanel";

		}


		// gets fired on clicking the creat button in the create game panel page
		public void CreateGameButtonWithNameClick ()
		{

				CreateRoom ();
		}


		// gets fired when clicking the join game buttoni n the main menu , leaves a room if in any, and sets the panels correctly respectivly.
		public void JoinGameButtonClick ()
		{
				PhotonNetwork.LeaveRoom ();
				readyToLoad = true;
				hasJoinedRoom = false;
				NetworkInterfaceState = "GameLobbyPanel";


		}

		void InstructionsScreen ()
		{
				Instructions.SetActive (true);
				GameCreatePanel.SetActive (false);
				GameLobbyPanel.SetActive (false);
				ConnectionDisplayPanel.SetActive (false);
				WaitingForPlayersPanel.SetActive (false);
		}

		public void InstructionsButton ()
		{
				hasJoinedRoom = false;
				NetworkInterfaceState = "InstructionsPanel";
		}

		// exits the application -> fired from clicking the quit button in Main Menu

		public void QuitGameButtonClick ()
		{
				Application.Quit ();
				Debug.Log (" should be quiting"); // laten staan, in editor quit hij anders niet.
		}


		// creates a room and joins it with the nam entered as long as its not empty and if it is, it will create a game with a random number between 101 and 100000, 
		// allso gets the game NAme from the input value component and saves this and calls a createRoom with this name
		public void CreateRoom ()
		{
				if (GameNameInput != null) {
						playerRoomName = GameNameInput.GetComponent<InputField> ().value;
						readyToLoad = true;
						NetworkInterfaceState = "WaitingForPlayersPanel";
						hasJoinedRoom = true;
				}
				if (playerRoomName != "") {
						hasJoinedRoom = true;
						PhotonNetwork.CreateRoom (playerRoomName);
	
				} else {
						playerRoomName = (Random.Range (1001, 100000)).ToString ();
						hasJoinedRoom = true;
						PhotonNetwork.CreateRoom (playerRoomName);

			
				}

		
		}

		public static string getGameState ()
		{

				string gameState = r_networkManager.playerState;

				return gameState;
		}

	





}




















