﻿using UnityEngine;
using System.Collections;

public class jetskiObstacleDestroy : MonoBehaviour
{


		public static float speed1 = -0.2f;


		// Update is called once per frame
		void Update ()
		{
				rigidbody.velocity += new Vector3 (0, speed1, speed1 + 0.05f);
		
				//remove object if he is not visible anymore
				if (transform.position.z < 125 && PhotonNetwork.isMasterClient) {
						PhotonNetwork.Destroy (this.gameObject);

				}
		}
}
