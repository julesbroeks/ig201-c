﻿using UnityEngine;
using System.Collections;

public class subPointRings : MonoBehaviour
{

	
		public GameObject collectSound;
		private GameObject holdSound;
		// Use this for initialization
		void Start ()
		{
	
		}

		void OnTriggerEnter (Collider other)
		{
				if (other.tag == "sub" && !PhotonNetwork.isMasterClient) {
						if (!PlayerHealth.doublePoints) {
								PhotonNetwork.Instantiate ("+5points", transform.position, Quaternion.identity, 0);
								RPCManager.AddPoints (5);
						} else {
								PhotonNetwork.Instantiate ("+10points", transform.position, Quaternion.identity, 0);
								RPCManager.AddPoints (10);
						}
						if (collectSound)
								holdSound = PhotonNetwork.Instantiate ("getHPSound", transform.position, Quaternion.identity, 0);
			
				}

		}
}
