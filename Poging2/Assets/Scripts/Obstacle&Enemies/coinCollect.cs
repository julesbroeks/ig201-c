﻿using UnityEngine;
using System.Collections;

public class coinCollect : MonoBehaviour
{

		public GameObject collectSound;
		private GameObject holdSound;

		void OnCollisionEnter (Collision other)
		{
				if (other.collider.tag == "jetski") {

						if (GetComponent<PhotonView> ().isMine) {
								PhotonNetwork.Destroy (gameObject);
								if (!PlayerHealth.doublePoints) {
										PhotonNetwork.Instantiate ("+2jetski", transform.position, Quaternion.identity, 0);
										RPCManager.AddPoints (2);
								} else {
										PhotonNetwork.Instantiate ("+5jetski", transform.position, Quaternion.identity, 0);
										RPCManager.AddPoints (5);
								}
						}
						if (collectSound)
								holdSound = PhotonNetwork.Instantiate ("getHPSound", transform.position, Quaternion.identity, 0);
				}
		}
}
