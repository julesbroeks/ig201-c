﻿using UnityEngine;
using System.Collections;

public class missRing : MonoBehaviour
{
		GameObject sub;

		public void Update ()
		{
				sub = GameObject.FindGameObjectWithTag ("sub");
		}

		public void OnTriggerEnter (Collider other)
		{
				if (other.tag == "sub" && !PhotonNetwork.isMasterClient) {

						RPCManager.MissRingFunction (10);
						PhotonNetwork.Instantiate ("-10sub", sub.transform.position, Quaternion.identity, 0);
				}
		}
}
