﻿using UnityEngine;
using System.Collections;

public class crateSubMovement : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		rigidbody.velocity = (new Vector3(0, -1.35f, objectMovement.speed));
		if (transform.position.z > -40 && GetComponent<PhotonView>().isMine) {
			
			PhotonView.Destroy(GetComponent<PhotonView>());
			PhotonNetwork.Destroy (this.gameObject);
		}
	}
}
