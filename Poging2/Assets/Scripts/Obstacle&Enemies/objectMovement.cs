﻿using UnityEngine;
using System.Collections;

public class objectMovement : MonoBehaviour {


	public static int speed = 10;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		rigidbody.velocity = (new Vector3(0, 0, speed));
		if (transform.position.z > -40 && GetComponent<PhotonView>().isMine) {

			PhotonView.Destroy(GetComponent<PhotonView>());
			PhotonNetwork.Destroy (this.gameObject);
				}
	}
}
