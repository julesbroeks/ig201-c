﻿using UnityEngine;
using System.Collections;

public class sinkCrate : MonoBehaviour
{

		public static bool sinking = false;
		private int sinkCheck = 0;

		// Use this for initialization
		void Start ()
		{
				RPCManager.sinkIt += this.SinkCrate;
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (sinkCheck == 1) {
						sinking = true;
						sinkCheck = 0;
				}
		}

		public void SinkCrate (int sink)
		{
				GetComponent<PhotonView> ().RPC ("SinkCrateRPC", PhotonTargets.All, new object[] {sink});
		}

		[RPC]
		void SinkCrateRPC (int sink)
		{
				sinkCheck += sink;
		}
}
