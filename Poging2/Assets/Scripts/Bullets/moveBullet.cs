﻿using UnityEngine;
using System.Collections;

public class moveBullet : MonoBehaviour {


	static float speed = 0.5f	;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		checkPosition();
		moveNormalBullet();
		moveSeekingBullet();

	
	}

	void checkPosition()
	{

		for(int i = 0; i < bullets.bulletList.Count; i++ )
		{
			// calling  recycle normal bullets ( not creating them again ) 
		if(	bullets.bulletList[i].bulleObject.transform.position.z <  - 120 )
			{
				RecycleBullets.recyleBulletNormal(i);
			}
		}

		for(int i = 0; i < seekingBulletes.seekingBulletList.Count; i++ )
		{
			// calling  recycle seeking bullets ( not creating them again ) 
			if (seekingBulletes.seekingBulletList[i].bulletObject.transform.position.z < -120 )
			{
				RecycleBullets.recycleSeekingBullet(i);
			}
		}	
	
	}

	void moveNormalBullet()
	{
		for(int i = 0; i < bullets.bulletList.Count; i++ )
		{
		
			Vector3 finalVectorMoveSpeedDirection = getCorrectSpeed(bullets.bulletList[i].directionVector) ; 
			bullets.bulletList[i].bulleObject.transform.position += finalVectorMoveSpeedDirection;
		}

	}
	void moveSeekingBullet()
	{
		for(int i = 0; i < seekingBulletes.seekingBulletList.Count; i++ )
		{
			if(seekingBulletes.seekingBulletList[i].hasTarget == true && seekingBulletes.seekingBulletList[i].targetObject != null){
				Vector3 finalMovingTargetVector =  getCorrectSeekingSpeed(seekingBulletes.seekingBulletList[i].targetObject,seekingBulletes.seekingBulletList[i].bulletObject);
				seekingBulletes.seekingBulletList[i].bulletObject.transform.position += finalMovingTargetVector;
			}
			else{
			Vector3 finalVectorMoveSpeedDirection =  getCorrectSpeed(seekingBulletes.seekingBulletList[i].directionVector);
			seekingBulletes.seekingBulletList[i].bulletObject.transform.position += finalVectorMoveSpeedDirection;
				seekingBulletes.seekingBulletList[i].bulletObject.transform.rotation = Quaternion.LookRotation(finalVectorMoveSpeedDirection);
			}
		}
	}

 	static Vector3 getCorrectSpeed(Vector3 directionVector)
	{
		Vector3 correctMoveDirectionAndSpeed;
		correctMoveDirectionAndSpeed = directionVector.normalized;
		correctMoveDirectionAndSpeed.Scale(new Vector3(speed,speed,speed));
		return correctMoveDirectionAndSpeed;

	}
	public static Vector3 getCorrectSeekingSpeed(GameObject targetObject,GameObject sourceObject)
	{


		Vector3 correctMoveDirectionAndSpeedOfTarget = new Vector3
			(targetObject.transform.position.x - sourceObject.transform.position.x,
			 targetObject.transform.position.y - sourceObject.transform.position.y,
			 targetObject.transform.position.z - sourceObject.transform.position.z);
		correctMoveDirectionAndSpeedOfTarget = correctMoveDirectionAndSpeedOfTarget.normalized;
		correctMoveDirectionAndSpeedOfTarget.Scale(new Vector3(speed,speed,speed));
		return correctMoveDirectionAndSpeedOfTarget;

	}


}
