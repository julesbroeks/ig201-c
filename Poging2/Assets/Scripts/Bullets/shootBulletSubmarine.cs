using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class shootBulletSubmarine : MonoBehaviour
{


		public GameObject submarine;
		public Camera submarineCamera;
		public static Vector3 CurrentRayDirection;
		public static GameObject targetObject;
		private float fireRate = 1;
		float timeSinceLastShot = 0;
		public static bool hasTarget = false;
		public GameObject shotSound;
		private GameObject holdSound;





		// Update is called once per frame
		void Update ()
		{

				// cursor position fixed to the small screen.
				Vector3 mousePosition = Input.mousePosition;

				if (mousePosition.x < Screen.width * 0.3) {
						Screen.showCursor = true; 
				} else {
						Screen.showCursor = false;
				}
				//

				//creates the ray//
				Ray ray = submarineCamera.ScreenPointToRay (Input.mousePosition);
				ray.origin = submarine.transform.position;
				CurrentRayDirection = ray.direction;
				RaycastHit rayhit;
				Physics.Raycast (ray, out rayhit);
		
				// MOuse trigger for shot.
				if (Input.GetMouseButtonDown (0) && timeSinceLastShot < Time.time && !PhotonNetwork.isMasterClient) {

						if (shotSound)
								holdSound = PhotonNetwork.Instantiate ("torpedoLaunch", transform.position, Quaternion.identity, 0);


						timeSinceLastShot = Time.time + fireRate;
						CurrentRayDirection = ray.direction;
						if (rayhit.collider != null) {
								if (rayhit.collider.gameObject.tag == "shark") {
									
										targetObject = rayhit.collider.gameObject;
										hasTarget = true;

									

								}
						} else {

								targetObject = submarine; 
								hasTarget = false;
						}

				
						bulletManager.FireBullet (CurrentRayDirection, targetObject, hasTarget);
			
	
						
				}


		}

}



    