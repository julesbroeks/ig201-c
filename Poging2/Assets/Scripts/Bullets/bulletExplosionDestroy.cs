﻿using UnityEngine;
using System.Collections;

public class bulletExplosionDestroy : MonoBehaviour
{

		// Use this for initialization
		void Start ()
		{
				StartCoroutine (DestroyExplosion ());
		}

		IEnumerator DestroyExplosion ()
		{
				while (true) {
			
						yield return new WaitForSeconds (1.0f);
						PhotonView.Destroy (this.gameObject);
						StopCoroutine (DestroyExplosion ());
				}
		}
}
