﻿using UnityEngine;
using System.Collections;

public class CreateBullets : MonoBehaviour {

		// creates a new normal bullet
	public static void createNormalNewBullet(Vector3 direction)
	{
		GameObject newNormalBullet = PhotonNetwork.Instantiate ("bulletPrefab", bulletManager.subPosition, Quaternion.LookRotation (direction), 0) as GameObject;
		bullets.bulletInfo BulletInstance = new bullets.bulletInfo (newNormalBullet, direction);
		bullets.bulletList.Add (BulletInstance);

	}
	 // creates a new seeking bullet
	public static void createSeekingNewBullet(Vector3 direction,GameObject targetObject,bool hasTarget)
	{

		GameObject newSeekingBullet = PhotonNetwork.Instantiate ("seekingBulletUpgraded", bulletManager.subPosition, Quaternion.LookRotation (direction), 0) as GameObject;
		seekingBulletes.bulletInfo seekingBullet = new seekingBulletes.bulletInfo (newSeekingBullet,targetObject,hasTarget,direction);
		seekingBulletes.seekingBulletList.Add (seekingBullet);
		
	}

}
