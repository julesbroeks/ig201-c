﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class bullets : ScriptableObject {

	public static List<bulletInfo> bulletList = new List<bulletInfo>();
	public static List<bulletInfo> bulletGarbage = new List<bulletInfo>();


	// pretty simple =)
	[System.Serializable]
	public class bulletInfo {

		public GameObject bulleObject;
		public Vector3 directionVector;



		public bulletInfo(GameObject bullet, Vector3 direction) {
		
			this.bulleObject = bullet;
			this.directionVector = direction;

		}
		

	

	}
}