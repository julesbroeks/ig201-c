﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class seekingBulletes : ScriptableObject {
	

	public static List<bulletInfo> seekingBulletList = new List<bulletInfo>();
	public static List<bulletInfo> seekingBulletGarbage = new List<bulletInfo>();
	
	// pretty simple =)
	[System.Serializable]
	public class bulletInfo {

		public Vector3 directionVector;
		public GameObject bulletObject;
		public GameObject targetObject;
		public bool hasTarget;
		
		
		
		public bulletInfo(GameObject bullet, GameObject targetObject,bool hasTarget,Vector3 directionVector) {
			
			this.bulletObject = bullet;
			this.targetObject = targetObject;
			this.hasTarget = hasTarget;
			this.directionVector = directionVector;
			
		}
		
		
		
		
	}
}