﻿using UnityEngine;
using System.Collections;

public class RecycleBullets : MonoBehaviour {

	public GameObject submarine;


	void Update()
	{

	}
	// recycles a normal bullet and reuses it
	public static  void recycleNormalBulletCreateNew(Vector3 direction)
	{

		bullets.bulletGarbage [0].bulleObject.transform.position = bulletManager.subPosition;
		bullets.bulletGarbage [0].bulleObject.transform.rotation = Quaternion.LookRotation (direction);
		bullets.bulletGarbage [0].directionVector = direction;
		bullets.bulletList.Add (bullets.bulletGarbage [0]);
		bullets.bulletGarbage.RemoveAt (0);
	}

	// recycles a seeking bullet
	public static void recycleSeekingBulletCreateNew(Vector3 direction,GameObject targetObject,bool hasTarget)
	{

		seekingBulletes.seekingBulletGarbage [0].bulletObject.transform.position = bulletManager.subPosition;
		seekingBulletes.seekingBulletGarbage [0].bulletObject.transform.rotation = Quaternion.LookRotation (direction);
		seekingBulletes.seekingBulletGarbage [0].targetObject = targetObject;
		seekingBulletes.seekingBulletGarbage[0].hasTarget = hasTarget;
		seekingBulletes.seekingBulletGarbage[0].directionVector = direction;
		seekingBulletes.seekingBulletList.Add (seekingBulletes.seekingBulletGarbage [0]);
		seekingBulletes.seekingBulletGarbage.RemoveAt (0);
		
	}

	// queus a normal bullet up for recycling
	public static void recyleBulletNormal(int i)
	{
		bullets.bulletGarbage.Add (bullets.bulletList [i]);
		bullets.bulletList.RemoveAt (i);
	}
	
	// ques a seeking bullet up for recysling
	public static void recycleSeekingBullet(int i)
	{
		seekingBulletes.seekingBulletGarbage.Add (seekingBulletes.seekingBulletList [i]);
		seekingBulletes.seekingBulletList.RemoveAt (i);
	}

}
