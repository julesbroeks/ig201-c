﻿using UnityEngine;
using System.Collections;

public class spawnShark : MonoBehaviour
{


		public GameObject submarine;

		void Start ()
		{
				if (!PhotonNetwork.isMasterClient) {

						StartCoroutine (SpawnSharks ());
				}
		}

		IEnumerator SpawnSharks ()
		{
				while (true) {
						GameObject newShark = PhotonNetwork.Instantiate ("tempShark", new Vector3 (Random.Range (submarine.transform.position.x - 7, submarine.transform.position.x + 7), submarine.transform.position.y - 5, submarine.transform.position.z), Quaternion.identity, 0);
						sharkManager.SharkSpawned (newShark);
						yield return new WaitForSeconds (Random.Range (14, 20));
				}
		}

}
