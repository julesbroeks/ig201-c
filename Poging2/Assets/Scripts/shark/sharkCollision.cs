﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class sharkCollision : MonoBehaviour
{

		GameObject[] sharkSpawns;
		bool seekingsMissleActive = false;

		void Start ()
		{
				UpgradeManager.powerUpReceived += PowerUpReceived;
				UpgradeManager.powerUpLost += PowerUpLost;
				sharkManager.newSharkSpawned += SetSharkCollisionBox;
		}

		public void SetSharkCollisionBox (GameObject shark)
		{
				if (seekingsMissleActive) {
						SetCollisionBoxShark (6, 18, shark);
				} else {
						SetCollisionBoxShark (1, 2, shark);
				}
		}

		public void PowerUpReceived (string powerUp)
		{
				if (powerUp == "seekingMissle") {
						seekingsMissleActive = true;
						ChangeSharkCollisionBox (18, 18);
				}
		}
	
		public void PowerUpLost (string powerUp)
		{
				if (powerUp == "seekingMissle") {
						ChangeSharkCollisionBox (6, 6);
						seekingsMissleActive = false;
				}
		}
	
		public void ChangeSharkCollisionBox (float width, float height)
		{

				sharkSpawns = GameObject.FindGameObjectsWithTag ("shark");
				foreach (GameObject sharks in sharkSpawns) {
						sharks.GetComponent<BoxCollider> ().size = new Vector3 (width, height, 16);
				}
		}

		void SetCollisionBoxShark (float width, float height, GameObject shark)
		{
				shark.GetComponent<BoxCollider> ().size = new Vector3 (width, height, 16);
		}
}
