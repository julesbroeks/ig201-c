﻿using UnityEngine;
using System.Collections;

public class sharkSpawnAtJetski : MonoBehaviour
{

		public static bool spawnShark = false;

		[RPC]
		void SpawnShark ()
		{
				

				SpawnSharkJetskiInstantiate ();
				

		}

		public void SpawnSharkJetski ()
		{
				if (!PhotonNetwork.isMasterClient) {
						GetComponent<PhotonView> ().RPC ("SpawnShark", PhotonTargets.All, new object[] {});
				}
		}

		public void SpawnSharkJetskiInstantiate ()
		{
				if (PhotonNetwork.isMasterClient) {
						PhotonNetwork.Instantiate ("sharkJetski", new Vector3 (-97.87f, 297.39f, 135.39f), Quaternion.Euler (0, 0, 0), 0);
				}
		}
}
