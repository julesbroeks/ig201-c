﻿using UnityEngine;
using System.Collections;

public class sharkJetskiMovement : MonoBehaviour
{

		GameObject jetski;
		// Update is called once per frame
		void Update ()
		{

				if (jetski == null) {
						jetski = GameObject.FindGameObjectWithTag ("jetski");
						
				}
				this.transform.position += GetSharkSpeedAndDirection (jetski);
				Debug.Log (jetski.transform.position);
		}

		Vector3 GetSharkSpeedAndDirection (GameObject targetObject)
		{
		
				transform.LookAt (targetObject.transform);
				Vector3 correctMoveDirectionAndSpeedOfTarget = new Vector3
				(targetObject.transform.position.x - this.transform.position.x,
				 targetObject.transform.position.y - this.transform.position.y,
				 targetObject.transform.position.z - this.transform.position.z);
				correctMoveDirectionAndSpeedOfTarget = correctMoveDirectionAndSpeedOfTarget.normalized;
				correctMoveDirectionAndSpeedOfTarget.Scale (new Vector3 (0.12f, 0.12f, 0.12f));
				return correctMoveDirectionAndSpeedOfTarget;
		
		}

		void OnCollisionEnter (Collision other)
		{
				if (other.gameObject.tag == "jetski") {
						if (UpgradeInput.invulnerable) {
								PhotonNetwork.Destroy (this.gameObject);
						} else {
								PhotonView.Destroy (this.gameObject);
								if (PhotonNetwork.isMasterClient) {
										RPCManager.AddPoints (-20);
										PhotonNetwork.Instantiate ("-20jetski", transform.position, Quaternion.identity, 0);
								}
			
						}
				}
		}
}


