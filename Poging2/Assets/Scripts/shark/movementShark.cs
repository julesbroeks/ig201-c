﻿using UnityEngine;
using System.Collections;

public class movementShark : MonoBehaviour
{

		public float sharkSpeed = 0.02f;
		public Transform dummyJetski;
		sharkSpawnAtJetski sharkSpawnScript;

		// Use this for initialization
		void Start ()
		{
				dummyJetski = GameObject.FindWithTag ("dummyJetski").transform;
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (transform.position.z >= dummyJetski.transform.position.z) {
						transform.Translate (new Vector3 (0, 0, -sharkSpeed));
				} else {
						transform.Translate (new Vector3 (0, sharkSpeed * 3, 0));
				}
				if (sharkSpawnScript == null && GameObject.FindGameObjectWithTag ("RPCmanager") != null) {
			
						sharkSpawnScript = GameObject.FindGameObjectWithTag ("RPCmanager").GetComponent<sharkSpawnAtJetski> ();
				}
		}

		void OnCollisionEnter (Collision other)
		{
				if (other.gameObject.tag == "dummyJetski") {
						sharkSpawnScript.SpawnSharkJetski ();
						PhotonView.Destroy (this.gameObject);
				}
		}
}
