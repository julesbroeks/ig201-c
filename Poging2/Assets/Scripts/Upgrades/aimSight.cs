﻿using UnityEngine;
using System.Collections;

public class aimSight : MonoBehaviour {
	
	public Sprite normalAimsight;
	public Sprite SeekingMissleAimsight;
	public Sprite SeekingMissleAimsightTarget;
	public GameObject submarine;
	public GameObject aimsightone;
	public GameObject aimsightTwee;
	public GameObject aimsightDrie;
	public Camera submarineCamera;
	Vector3 aimsightPosition;
	Vector3 aimsightPositionTwee;
	Vector3 aimsightPositionDrie;
	public static bool seekingActivated = false;
	public static bool mouseActive = false;

	// Use this for initialization
	void Start () {
		if(!PhotonNetwork.isMasterClient){
		aimsightone =		 Instantiate(aimsightone, submarine.transform.position,Quaternion.identity) as GameObject;
		aimsightTwee =	 Instantiate(aimsightTwee,submarine.transform.position,Quaternion.identity) as GameObject;
		aimsightDrie =	 Instantiate(aimsightDrie,submarine.transform.position,Quaternion.identity) as GameObject;
			UpgradeManager.powerUpReceived += powerUpReceivedForAim;
			UpgradeManager.powerUpLost += powerUpLostForAim;
		}
	}


	public void powerUpReceivedForAim(string powerUp)
	{
		if(powerUp == "seekingMissle")
		{
			aimsightone.GetComponent<SpriteRenderer>().sprite = SeekingMissleAimsight;
			seekingActivated = true;
		}
	}

	public void powerUpLostForAim(string powerUp)
	{
		if(powerUp == "seekingMissle")
		{
			aimsightone.GetComponent<SpriteRenderer>().sprite = normalAimsight;
			seekingActivated = false;
		}
	}

	void Update()
	{

		Vector3 mousePosition = Input.mousePosition;
		
		if ( mousePosition.x < Screen.width * 0.3) {
			Screen.showCursor = true; 
		} else if (!mouseActive){
			Screen.showCursor = false;
		}
		if(!PhotonNetwork.isMasterClient){

	
		aimsightPositionTwee = new Vector3(submarine.transform.position.x + ( shootBulletSubmarine.CurrentRayDirection.x * 10) ,transform.position.y +  ( shootBulletSubmarine.CurrentRayDirection.y * 10),transform.position.z +  ( shootBulletSubmarine.CurrentRayDirection.z * 10));
		aimsightPositionDrie = new Vector3(submarine.transform.position.x + ( shootBulletSubmarine.CurrentRayDirection.x * 4) ,transform.position.y +  (shootBulletSubmarine.CurrentRayDirection.y * 4),transform.position.z +  ( shootBulletSubmarine.CurrentRayDirection.z * 4));
		aimsightone.transform.position =  aimsightPosition;
		aimsightTwee.transform.position = aimsightPositionTwee;
		aimsightDrie.transform.position = aimsightPositionDrie;

		if(seekingActivated)
			{
			
				Ray ray = submarineCamera.ScreenPointToRay(Input.mousePosition);
				ray.origin = submarine.transform.position;
				RaycastHit rayhit;
				if(Physics.Raycast(ray, out rayhit))
				{
					if (rayhit.collider.gameObject.tag =="shark" )
					{
						aimsightone.GetComponent<SpriteRenderer>().sprite = SeekingMissleAimsightTarget;
						aimsightPosition = rayhit.collider.gameObject.transform.position;
					
					}
					else { 
						aimsightone.GetComponent<SpriteRenderer>().sprite = SeekingMissleAimsight;
						aimsightPosition = new Vector3(submarine.transform.position.x + (  shootBulletSubmarine.CurrentRayDirection.x * 50) ,transform.position.y +  ( shootBulletSubmarine.CurrentRayDirection.y * 50),transform.position.z +  ( shootBulletSubmarine.CurrentRayDirection.z * 50));
					}
				}
			}
			else {

				aimsightPosition = new Vector3(submarine.transform.position.x + (  shootBulletSubmarine.CurrentRayDirection.x * 50) ,transform.position.y +  ( shootBulletSubmarine.CurrentRayDirection.y * 50),transform.position.z +  ( shootBulletSubmarine.CurrentRayDirection.z * 50));
			}

		
	}
	}





}
