﻿using UnityEngine;
using System.Collections;

public class StartPowerUps : MonoBehaviour
{
		int a = 1;
		// Use this for initialization
		void Start ()
		{

				if (PhotonNetwork.isMasterClient) {
						StartCoroutine (PowerUpsJetski ());
				} else if (!PhotonNetwork.isMasterClient) {
						StartCoroutine (StartSupPowerUp ());
				}


		}
	
		// Update is called once per frame
		void Update ()
		{
		}
	
		IEnumerator PowerUpsJetski ()
		{
				while (true) {


						PhotonNetwork.Instantiate ("upgradeBoxJetski", new Vector3 (Random.Range (-95, -80), 336.48f, 154.38f), Quaternion.identity, 0);
						yield return new WaitForSeconds (10);

				}
		}

		IEnumerator StartSupPowerUp ()
		{ 
				while (a == 1) {
						yield return new WaitForSeconds (2.5f);
						StartCoroutine (PowerUpsSub ());
						a++;
				}
		}

		IEnumerator PowerUpsSub ()
		{
				while (true) {

						PhotonNetwork.Instantiate ("upgradeBoxSub", new Vector3 (Random.Range (15, -10), Random.Range (10, 30), -120), Quaternion.identity, 0);
						yield return new WaitForSeconds (10);
				}
		}




}

