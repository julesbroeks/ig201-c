﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpgradeCounter : MonoBehaviour
{


		public int counter = 5;
		// Use this for initialization
		void Start ()
		{
	

				UpgradeCounterDelegates.ChangeCount += this.ChangeTheCounterOnBothPlayers;

		}
	
		public void ChangeTheCounterOnBothPlayers (int amount)
		{
				GetComponent<PhotonView> ().RPC ("UpdateOtherPlayerCounter", PhotonTargets.All, new object[] {amount});
		}

		[RPC]
		void UpdateOtherPlayerCounter (int amount)
		{
				counter += amount;
				ChangeTheCounterText ();
		}

		public void ChangeTheCounterText ()
		{
				GameObject.FindGameObjectWithTag ("powerUpCount").GetComponent<Text> ().text = counter.ToString ();
		}



}
