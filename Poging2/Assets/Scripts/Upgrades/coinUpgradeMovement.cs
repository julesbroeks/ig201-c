﻿using UnityEngine;
using System.Collections;

public class coinUpgradeMovement : MonoBehaviour
{

		float speed = -0.001f;
	
	
		// Update is called once per frame
		void Update ()
		{
				rigidbody.velocity += new Vector3 (0, speed, speed);
		
				//remove object if he is not visible anymore
				if (transform.position.z < 125 && PhotonNetwork.isMasterClient) {
						PhotonNetwork.Destroy (this.gameObject);
			
				}
		}
}
