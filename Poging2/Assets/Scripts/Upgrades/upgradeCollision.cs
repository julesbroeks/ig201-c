﻿using UnityEngine;
using System.Collections;

public class upgradeCollision : MonoBehaviour
{
		GameObject sub;

		void Update ()
		{
				sub = GameObject.FindGameObjectWithTag ("sub");
		}

		void OnCollisionEnter (Collision other)
		{
				if (other.gameObject.tag != null) {

						if (other.gameObject.tag == "jetski" && GetComponent<PhotonView> ().isMine) {
								UpgradeCounterDelegates.ChangeTheCount (1);
								PhotonNetwork.Destroy (this.gameObject);
								PhotonNetwork.Instantiate ("+1jetski", transform.position, Quaternion.identity, 0);

			
						}
						if (other.gameObject.tag == "sub" && GetComponent<PhotonView> ().isMine) {
								UpgradeCounterDelegates.ChangeTheCount (1);
								PhotonNetwork.Destroy (this.gameObject);
								PhotonNetwork.Instantiate ("+1sub", sub.transform.position, Quaternion.identity, 0);
			
			
						}
				}
		}
}
