﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpgradeInput : MonoBehaviour
{
	
		public static bool invulnerable;
		public Text powerUpText;
		public Text powerUpCounterText;
		UpgradeCounter upgradeCounterScript;
		movementSub subMoveSpeed ;
		movementJetski jetskiMoveSpeed ;
		bool upgradeActive = false;
		public GameObject usePUSound;
		private GameObject holdSound;
	
		void Start ()
		{
		
		
		
				upgradeCounterScript = GameObject.FindGameObjectWithTag ("RPCmanager").GetComponent<UpgradeCounter> ();
		
				//subMoveSpeed = GameObject.FindGameObjectWithTag ("sub").GetComponent<movementSub> ();
				//jetskiMoveSpeed = GameObject.FindGameObjectWithTag ("jetski").GetComponent<movementJetski> ();
		
		}
	
		void Update ()
		{
		
				if (upgradeCounterScript == null) {
						upgradeCounterScript = GameObject.FindGameObjectWithTag ("RPCmanager").GetComponent<UpgradeCounter> ();
				}
				if (subMoveSpeed == null && GameObject.FindGameObjectWithTag ("sub") != null) {
			
						subMoveSpeed = GameObject.FindGameObjectWithTag ("sub").GetComponent<movementSub> ();
				}
				if (jetskiMoveSpeed == null && GameObject.FindGameObjectWithTag ("jetski") != null) {
						jetskiMoveSpeed = GameObject.FindGameObjectWithTag ("jetski").GetComponent<movementJetski> ();
				}
		
				if (Input.GetKeyDown (KeyCode.Alpha1) && upgradeCounterScript.counter > 0 && upgradeActive == false) {
			
						if (!PhotonNetwork.isMasterClient) {
								UpgradeManager.ReceivedNewPowerUp ("seekingMissle");
								UpgradeCounterDelegates.ChangeTheCount (-1);
								upgradeActive = true;
								StopCoroutine (SeekingMissle ());
								StartCoroutine (SeekingMissle ());
				
								if (usePUSound)
										holdSound = PhotonNetwork.Instantiate ("usePowerUpSound", transform.position, Quaternion.identity, 0);
						} else { 
								// invulnerable
								UpgradeInput.invulnerable = true;
								StartCoroutine (InvulnerableUpgrade ());
								upgradeActive = true;
				
								if (usePUSound)
										holdSound = PhotonNetwork.Instantiate ("usePowerUpSound", transform.position, Quaternion.identity, 0);
						}
				}
		
				if (Input.GetKeyDown (KeyCode.Alpha2) && upgradeCounterScript.counter > 0 && upgradeActive == false) {
			
						PlayerHealth.doublePoints = true;
						UpgradeCounterDelegates.ChangeTheCount (-1);
						upgradeActive = true;
						StopCoroutine (DoublePoints ());
						StartCoroutine (DoublePoints ());
			
						if (usePUSound)
								holdSound = PhotonNetwork.Instantiate ("usePowerUpSound", transform.position, Quaternion.identity, 0);
			
				}
				if (Input.GetKeyDown (KeyCode.Alpha3) && upgradeCounterScript.counter > 0 && upgradeActive == false) {
			
						if (!PhotonNetwork.isMasterClient) {
								subMoveSpeed.moveSpeed = 3;
								UpgradeCounterDelegates.ChangeTheCount (-1);
								upgradeActive = true;
								StopCoroutine (MoveSpeedSub ());
								StartCoroutine (MoveSpeedSub ());
				
								if (usePUSound)
										holdSound = PhotonNetwork.Instantiate ("usePowerUpSound", transform.position, Quaternion.identity, 0);
						} else {
								jetskiMoveSpeed.moveSpeedJetski = 3;
								UpgradeCounterDelegates.ChangeTheCount (-1);
								upgradeActive = true;
								StopCoroutine (MoveSpeedJetski ());
								StartCoroutine (MoveSpeedJetski ());
				
								if (usePUSound)
										holdSound = PhotonNetwork.Instantiate ("usePowerUpSound", transform.position, Quaternion.identity, 0);
				
						}
			
				}
		
		}
	
		IEnumerator SeekingMissle ()
		{
				StartCoroutine (UpgradeCountdownTimer ());
				powerUpText.text = " Target Seeking Missle Activated ! ";
				int i = 1;
				while (i == 1) {
			
						yield return new WaitForSeconds (4);
						powerUpText.text = "";
						yield return new WaitForSeconds (6);
						UpgradeManager.DeletePowerUp ("seekingMissle");
						upgradeActive = false;
						i++;
						StopCoroutine (UpgradeCountdownTimer ());
						StopCoroutine (SeekingMissle ());
				}
		
		}
	
		IEnumerator DoublePoints ()
		{
				StartCoroutine (UpgradeCountdownTimer ());
				powerUpText.text = " Double Points Activated ! ";
				int i = 1;
				while (i == 1) {
						yield return new WaitForSeconds (4);
						powerUpText.text = "";
						yield return new WaitForSeconds (6);
						PlayerHealth.doublePoints = false;
						upgradeActive = false;
						i++;
						StopCoroutine (UpgradeCountdownTimer ());
						StopCoroutine (DoublePoints ());
				}
		}
	
		IEnumerator MoveSpeedSub ()
		{
				StartCoroutine (UpgradeCountdownTimer ());
				powerUpText.text = "Supersonic Speed Activated ! ";
				int i = 1;
				while (i == 1) {
						yield return new WaitForSeconds (4);
						powerUpText.text = "";
						yield return new WaitForSeconds (6);
						subMoveSpeed.moveSpeed = 2;
						upgradeActive = false;
						i++;
						StopCoroutine (UpgradeCountdownTimer ());
						StopCoroutine (MoveSpeedSub ());
			
				}
		}
	
		IEnumerator MoveSpeedJetski ()
		{
				StartCoroutine (UpgradeCountdownTimer ());
				powerUpText.text = "Supersonic Speed Activated ! ";
				int i = 1;
				while (i == 1) {
						yield return new WaitForSeconds (4);
						powerUpText.text = "";
						yield return new WaitForSeconds (6);
						jetskiMoveSpeed.moveSpeedJetski = 2;
						upgradeActive = false;
						i++;
						StopCoroutine (UpgradeCountdownTimer ());
						StopCoroutine (MoveSpeedJetski ());
			
				}
		}

		IEnumerator InvulnerableUpgrade ()
		{
				StartCoroutine (UpgradeCountdownTimer ());
				powerUpText.text = "Invulnerable Upgrade Active !";
				int i = 1;
				while (i == 1) {
						yield return new WaitForSeconds (4);
						powerUpText.text = "";
						yield return new WaitForSeconds (6);
						UpgradeInput.invulnerable = false;
						upgradeActive = false;
						i++;
						StopCoroutine (UpgradeCountdownTimer ());
						StopCoroutine (InvulnerableUpgrade ());
			
				}
		}
	
		IEnumerator UpgradeCountdownTimer ()
		{
		
				int i = 10;
				powerUpCounterText.text = i.ToString ();
				while (i > 0) {
			
						yield return new WaitForSeconds (1);
						i--;
						powerUpCounterText.text = i.ToString ();
			
				}
		
		}
	
	
}